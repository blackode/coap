# IsX

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add `is_x` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:is_x, "~> 0.1.0"}]
    end
    ```

  2. Ensure `is_x` is started before your application:

    ```elixir
    def application do
      [applications: [:is_x]]
    end
    ```

