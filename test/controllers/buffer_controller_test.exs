defmodule Coap.BufferControllerTest do
  use Coap.ConnCase

  alias Coap.Buffer
  @valid_attrs %{}
  @invalid_attrs %{}

  test "lists all entries on index", %{conn: conn} do
    conn = get conn, buffer_path(conn, :index)
    assert html_response(conn, 200) =~ "Listing buffer"
  end

  test "renders form for new resources", %{conn: conn} do
    conn = get conn, buffer_path(conn, :new)
    assert html_response(conn, 200) =~ "New buffer"
  end

  test "creates resource and redirects when data is valid", %{conn: conn} do
    conn = post conn, buffer_path(conn, :create), buffer: @valid_attrs
    assert redirected_to(conn) == buffer_path(conn, :index)
    assert Repo.get_by(Buffer, @valid_attrs)
  end

  test "does not create resource and renders errors when data is invalid", %{conn: conn} do
    conn = post conn, buffer_path(conn, :create), buffer: @invalid_attrs
    assert html_response(conn, 200) =~ "New buffer"
  end

  test "shows chosen resource", %{conn: conn} do
    buffer = Repo.insert! %Buffer{}
    conn = get conn, buffer_path(conn, :show, buffer)
    assert html_response(conn, 200) =~ "Show buffer"
  end

  test "renders page not found when id is nonexistent", %{conn: conn} do
    assert_error_sent 404, fn ->
      get conn, buffer_path(conn, :show, -1)
    end
  end

  test "renders form for editing chosen resource", %{conn: conn} do
    buffer = Repo.insert! %Buffer{}
    conn = get conn, buffer_path(conn, :edit, buffer)
    assert html_response(conn, 200) =~ "Edit buffer"
  end

  test "updates chosen resource and redirects when data is valid", %{conn: conn} do
    buffer = Repo.insert! %Buffer{}
    conn = put conn, buffer_path(conn, :update, buffer), buffer: @valid_attrs
    assert redirected_to(conn) == buffer_path(conn, :show, buffer)
    assert Repo.get_by(Buffer, @valid_attrs)
  end

  test "does not update chosen resource and renders errors when data is invalid", %{conn: conn} do
    buffer = Repo.insert! %Buffer{}
    conn = put conn, buffer_path(conn, :update, buffer), buffer: @invalid_attrs
    assert html_response(conn, 200) =~ "Edit buffer"
  end

  test "deletes chosen resource", %{conn: conn} do
    buffer = Repo.insert! %Buffer{}
    conn = delete conn, buffer_path(conn, :delete, buffer)
    assert redirected_to(conn) == buffer_path(conn, :index)
    refute Repo.get(Buffer, buffer.id)
  end
end
