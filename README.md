# Getting Started

```elixir
$ iex -S mix phoenix.start

iex> Ahamtech.start

```

## Installation


  1. Add `coap` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [{:coap, "~> 0.1.0"}]
    end
    ```

  2. Ensure `coap` is started before your application:

    ```elixir
    def application do
      [applications: [:coap]]
    end
    ```
