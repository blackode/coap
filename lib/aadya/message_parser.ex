defmodule Aadya.MessageParser do
  @moduledoc ~S"""
  CoapMessage documentation
  encoding an decoding for CoAP v1 messages
  """
  import Aadya.Iana, only: [content_formats: 0,decode_enum: 2, decode_enum: 3, encode_enum: 2, encode_enum: 3]
  require Record
  use Aadya.Header

  @version 1
  @option_if_match 1
  @option_uri_host 3
  @option_etag 4
  @option_if_none_match 5
  @option_observe 6 #. % draft-ietf-core-observe-1
  @option_uri_port 7
  @option_location_path 8
  @option_uri_path 11
  @option_content_format 12
  @option_max_age 14
  @option_uri_query 15
  @option_accept 17
  @option_location_query 20
  @option_block2 23 
  @option_block1 27
  @option_proxy_uri 35
  @option_proxy_scheme 39
  @option_size1 60

  # % empty message only contains the 4-byte header

  def decode(<<@version::2,type::2, 0::4, 0::3, 0::5,msgId::16>>) do
    coap_message(coap_message,type: decode_type(type), id: msgId)
  end

  def decode(<<@version::2, type::2, tKL::4, class::3, code::5, msgId::16, token::bytes-size(tKL) , tail::bytes>>) do
    {options, payload} = decode_option_list(tail)
    coap_message(
        coap_message,
           type: decode_type(type),
           method: decode_enum(methods(), {class, code}),
           id: msgId,
           token: token,
           options: options,
           payload: payload )
  end

  # % empty message
  def encode(coap_message(type: type, method: :nil, id: msgId ) = msg) do
    <<@version::2, (encode_type(type))::2, 0::4, 0::3, 0::5, msgId::16>>
  end

  def encode(coap_message(type: type, method: method, id: msgId, token: token, options: options, payload: payload )=msg) do
    tKL = byte_size(token)
    {class, code} = encode_enum(methods(), method)
    tail = encode_option_list(options, payload)
    <<@version::2, (encode_type(type))::2, tKL::4, class::3, code::5, msgId::16, token::bytes-size(tKL), tail::bytes>>
  end

  # % shortcut function for reset generation
  def message_id(<<_::16, msgId::16, _tail::bytes>>), do:  msgId
  def message_id(coap_message(id: msgId )=msg), do:  msgId

  def decode_type(0), do: :con
  def decode_type(1), do: :non
  def decode_type(2), do: :ack
  def decode_type(3), do: :reset

  def encode_type(:con), do: 0
  def encode_type(:non), do: 1
  def encode_type(:ack), do: 2
  def encode_type(:reset), do: 3

  def methods() do [
   # % RFC 7252
   # % atom indicate a request
   {{0,01}, :get},
   {{0,02}, :post},
   {{0,03}, :put},
   {{0,04}, :delete},
   # % success is a tuple {ok, ...}
   {{2,01}, {:ok, :created}},
   {{2,02}, {:ok, :deleted}},
   {{2,03}, {:ok, :valid}},
   {{2,04}, {:ok, :changed}},
   {{2,05}, {:ok, :content}},
   {{2,31}, {:ok, :continue}} ,# % block
   # % error is a tuple {error, ...}
   {{4,00}, {:error, :bad_request}},
   {{4,01}, {:error, :uauthorized}},
   {{4,02}, {:error, :bad_option}},
   {{4,03}, {:error, :forbidden}},
   {{4,04}, {:error, :not_found}},
   {{4,05}, {:error, :method_not_allowed}},
   {{4,06}, {:error, :not_acceptable}},
   {{4,08}, {:error, :request_entity_incomplete}}, #% block
   {{4,12}, {:error, :precondition_failed}},
   {{4,13}, {:error, :request_entity_too_large}},
   {{4,15}, {:error, :unsupported_content_format}},
   {{5,00}, {:error, :internal_server_error}},
   {{5,01}, {:error, :not_implemented}},
   {{5,02}, {:error, :bad_gateway}},
   {{5,03}, {:error, :service_unavailable}},
   {{5,04}, {:error, :gateway_timeout}},
   {{5,05}, {:error, :proxying_not_supported}}]
  end

  # % option parsing is based on Patrick's CoAP Message Parsing in Erlang
  # % https://gist.github.com/azdle/b2d477ff183b8bbb0aa0

  def decode_option_list(tail) do
    decode_option_list(tail, 0, [])
  end


  def decode_option_list(<<>>, _lastNum, optionList) do
    {optionList, <<>>}
  end

  def decode_option_list(<< 0xFF, payload::bytes >>, _lastNum, optionList) do
    {optionList, payload}
  end

  def decode_option_list(<<delta::4, len::4, tail::bytes>>, lastNum, optionList) do
    # IO.inspect [ delta, len, tail]

    {tail1, optNum} = cond do
      delta < 13 ->
        {tail, lastNum + delta}
      delta == 13 ->
        <<extOptNum, newTail1::bytes>> = tail
        {newTail1, lastNum + extOptNum + 13}
      delta == 14 ->
        <<extOptNum::16, newTail1::bytes>> = tail
        {newTail1, lastNum + extOptNum + 269}
    end
    {tail2, optLen} = cond do
      len < 13 ->
        {tail1, len}
      len == 13 ->
        <<extOptLen, newTail2::bytes>> = tail1
        {newTail2, extOptLen + 13}
      len == 14 ->
        <<extOptLen::16, newTail2::bytes>> = tail1
        {newTail2, extOptLen + 269}
    end

    # IO.inspect [tail2, optLen]

    case tail2 do
      <<optVal::bytes-size(optLen), nextOpt::bytes>> ->
        decode_option_list(nextOpt, optNum, append_option(decode_option(optNum, optVal), optionList))
      <<>> ->
        decode_option_list(<<>>, optNum, append_option(decode_option(optNum, <<>>), optionList))
    end
  end

  # % put options of the same id into one list
  def append_option({sameOptId, optVal2}, [{sameOptId, optVal1} | optionList]) do
    case is_repeatable_option(sameOptId) do
      true ->
        # % we must keep the order
        [{sameOptId, optVal1++[optVal2]} | optionList]
      false ->
        throw({:error, Atom.to_charlist(sameOptId)++" is not repeatable"})
    end
  end

  def append_option({optId2, optVal2}, optionList) do
    case is_repeatable_option(optId2) do
      true -> [{optId2, [optVal2]} | optionList]
      false -> [{optId2, optVal2} | optionList]
    end
  end

  def encode_option_list(options, <<>>), do: options |>  encode_option_list1

  def encode_option_list(options, payload) do
    <<(encode_option_list1(options))::bytes, 0xFF, payload::bytes >>
  end

  def encode_option_list1(options) do
    options1 = encode_options(options, [])
    # % sort before encoding so we can calculate the deltas
    # % the sort is stable; it maintains relative order of values with equal keys
    encode_option_list(:lists.keysort(1, options1), 0, <<>>)
  end

  def encode_options([{_optId, :nil} | optionList], acc) do
    encode_options(optionList, acc)
  end

  def encode_options([{optId, optVal} | optionList], acc) do
    case is_repeatable_option(optId) do
      true ->
        encode_options(optionList, split_and_encode_option({optId, optVal}, acc))
      false ->
        encode_options(optionList, [encode_option({optId, optVal}) | acc])
    end
  end

  def encode_options([], acc), do:  acc

  def split_and_encode_option({optId, [:nil | optVals]}, acc) do
    split_and_encode_option({optId, optVals}, acc)
  end

  def split_and_encode_option({optId, [optVal1 | optVals]}, acc) do
    # % we must keep the order
    [encode_option({optId, optVal1}) | split_and_encode_option({optId, optVals}, acc)]
  end

  def split_and_encode_option({_optId, []}, acc), do: acc

  def encode_option_list([{optNum, optVal} | optionList], lastNum, acc) do
    {delta, extNum} = cond do
      optNum - lastNum >= 269 ->
        {14, <<(optNum - lastNum - 269)::16>>}
      optNum - lastNum >= 13 ->
        {13, <<(optNum - lastNum - 13)>>}
      true ->
        {optNum - lastNum, <<>>}
    end
    {len, extLen} = cond do
      byte_size(optVal) >= 269 ->
        {14, <<(byte_size(optVal) - 269)::16>>}
      byte_size(optVal) >= 13 ->
        {13, <<(byte_size(optVal) - 13)>>}
      true ->
        {byte_size(optVal), <<>>}
    end

    acc2 = <<acc::bytes, delta::4, len::4, extNum::bytes, extLen::bytes, optVal::bytes>>
    encode_option_list(optionList, optNum, acc2)
  end

  def encode_option_list([], _lastNum, acc), do: acc
  def is_repeatable_option(:if_match), do: true
  def is_repeatable_option(:etag), do: true
  def is_repeatable_option(:location_path), do: true
  def is_repeatable_option(:uri_path), do: true
  def is_repeatable_option(:uri_query), do: true
  def is_repeatable_option(:location_query), do: true
  def is_repeatable_option(_else), do: false

  # % RFC 7252
  def decode_option(@option_if_match, optVal), do: {:if_match, optVal}
  def decode_option(@option_uri_host, optVal), do: {:uri_host, optVal}
  def decode_option(@option_etag, optVal), do: {:etag, optVal}
  def decode_option(@option_if_none_match, <<>>), do: {:if_none_match, true}
  def decode_option(@option_uri_port, optVal), do: {:uri_port, :binary.decode_unsigned(optVal)}
  def decode_option(@option_location_path, optVal), do: {:location_path, optVal}
  def decode_option(@option_uri_path, optVal), do: {:uri_path, optVal}
  def decode_option(@option_content_format, optVal) do
    num = :binary.decode_unsigned(optVal)
    {:content_format, decode_enum(content_formats(), num, num)}
  end

  def decode_option(@opotion_max_age, optVal), do: {:max_age, :binary.decode_unsigned(optVal)}
  def decode_option(@option_uri_query, optVal), do: {:uri_query, optVal}
  def decode_option(@option_accept, optVal), do: {'accept', :binary.decode_unsigned(optVal)}
  def decode_option(@option_location_query, optVal), do: {:location_query, optVal}
  def decode_option(@option_proxy_uri, optVal), do: {:proxy_uri, optVal}
  def decode_option(@option_proxy_scheme, optVal), do: {:proxy_scheme, optVal}
  def decode_option(@option_size, optVal), do: {:size1,:binary.decode_unsigned(optVal)}
  # % draft-ietf-core-observe-1

  def decode_option(@option_observe, optVal), do: {:observe, :binary.decode_unsigned(optVal)}
  # % draft-ietf-core-block-17

  def decode_option(@option_block2, optVal), do: {:block2, decode_block(optVal)}
  def decode_option(@option_block1, optVal), do: {:block1, decode_block(optVal)}
  # % unknown option
  def decode_option(optNum, optVal), do: {optNum, optVal}
  def decode_block(<<num::4, m::1, sizEx::3>>), do: decode_block1(num, m, sizEx)
  def decode_block(<<num::12, m::1, sizEx::3>>), do: decode_block1(num, m, sizEx)
  def decode_block(<<num::28, m::1, sizEx::3>>), do: decode_block1(num, m, sizEx)

  def decode_block1(num, m, sizEx) do
    {num,cond do
       m == 0 -> false
       true -> true
    end,
    trunc(:math.pow(2, sizEx+4))}
  end

  # % RFC 7252
  def encode_option({:if_match, optVal}), do: {@option_if_match, optVal}
  def encode_option({:uri_host, optVal}), do: {@optin_uri_host, optVal}
  def encode_option({:etag, optVal}), do: {@option_etag, optVal}
  def encode_option({:if_none_match, true}), do: {@option_if_none_match, <<>>}
  def encode_option({:uri_port, optVal}), do: {@option_uri_port, :binary.encode_unsigned(optVal)}
  def encode_option({:location_path, optVal}), do: {@option_location_path, optVal}
  def encode_option({:uri_path, optVal}), do: {@option_uri_path, optVal}
  def encode_option({:content_format, optVal}) when is_integer(optVal) do
    {@option_content_format, :binary.encode_unsigned(optVal)}
  end
  def encode_option({:content_format, optVal}) do# {{{
    num = encode_enum(content_formats(), optVal)
    {@option_content_format, :binary.encode_unsigned(num)}
  end# }}}

  def encode_option({:max_age, optVal}), do: {@opton_max_age, :binary.encode_unsigned(optVal)}
  def encode_option({:uri_query, optVal}), do: {@option_uri_query, optVal}
  def encode_option({'accept', optVal}), do: {@option_accept, :binary.encode_unsigned(optVal)}
  def encode_option({:location_query, optVal}), do: {@option_locatio_query, optVal}
  def encode_option({:proxy_uri, optVal}), do: {@option_proxy_uri, optVal}
  def encode_option({:proxy_scheme, optVal}), do: {@option_proxy_scheme, optVal}
  def encode_option({:size1, optVal}), do: {@opton_size, :binary.encode_unsigned(optVal)}

  # % draft-ietf-core-observe-16
  def encode_option({:observe, optVal}), do: {@option_observe, :binary.encode_unsigned(optVal)}

  # % draft-ietf-core-block-17
  def encode_option({:block2, optVal}), do: {@option_block2, encode_block(optVal)}
  def encode_option({:block1, optVal}), do: {@option_block1, encode_block(optVal)}

  # % unknown option

  def encode_block({num, more, size}) do# {{{
    encode_block1(num, cond do
                  more -> 1
                  1==1 -> 0
                  end,
    trunc(log2(size))-4)
  end

  def encode_block1(num, m, sizEx) when num < 16 do
    <<num::4, m::1, sizEx::3>>
  end

  def encode_block1(num, m, sizEx) when num < 4096 do
    <<num::12, m::1, sizEx::3>>
  end

  def encode_block1(num, m, sizEx) do
    <<num::28, m::1, sizEx::3>>
  end

  # % log2 is not available in R16B
  #
  def log2(x), do: :math.log(x) / :math.log(2)

  # -include_lib("eunit/include/eunit.hrl")
  import ExUnit.Assertions

  # % note that the options below must be sorted by the option numbers
  def codec_test_() do
    [
      test_codec(coap_message(type: :reset, id: 0, options: [] )),
      test_codec(coap_message(type: :con, method: :get, id: 100, options: [{:block1, {0,true,128}}, {:observe, 1}] )),
      test_codec(coap_message(type: :non, method: :put, id: 200, token: <<"token">>,
                 options: [{:uri_path,[<<".well-known">>, <<"core">>]}] )),
      test_codec(coap_message(type: :non, method: {:ok, 'content'}, id: 300, token:  <<"token">>,
                          payload: <<"<url>">>, options: [{:content_format, <<"application/link-format">>}, {:uri_path,[<<".well-known">>, <<"core">>]}] ))
                      ]
  end

  def test_codec(message) do
    message2 = encode(message)
    message1 = decode(message2)
    assert(message == message1)
  end

end# }}}
