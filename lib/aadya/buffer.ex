defmodule Aadya.Buffer do
  require Logger

  def start( client,payload) do
    Logger.info "Buffer start - client"
    :ets.insert( :buffer, { client, payload})
  end

  def append( client,payload) do
    [{_,data}]=:ets.lookup( :buffer, client)
    :ets.insert( :buffer, { client, data <> payload})
  end

  def stop(client) do
    :ets.delete( :buffer, client)
 end

  def get(client, seq, size ) do
    skip = seq * size
    case :ets.lookup(:buffer,client) do
      [{_,payload}] ->

        if( (byte_size(payload)-skip) < size) do
          <<
            _   :: binary-size(skip),
            pay :: binary
          >> = payload

          bool = false

        else

          <<
            _   :: binary-size(skip),
            pay :: binary-size(size),
            _   :: binary
          >> = payload

          bool = true
        end

        [{pay, bool}]
      [] ->
        []
    end
  end

  def get_all( client) do
    [{_,pay}] = :ets.lookup :buffer,client
    pay
  end
end
