defmodule Aadya.ObserverSup do
  require Record
  use GenServer
  
  def start_link(_tyupe) do
    Supervisor.start_link(__MODULE__, [],name: __MODULE__)
  end
  
  def init([])  do 
    Process.flag(:trap_exit, true)
    {:ok, {{:one_for_one, 3, 10}, []}}
  end

  def observer_start(sock,peer,port,id,token,options) do
    Supervisor.start_child(Aadya.ObserverSup,{{sock,peer,port},{Aadya.Observer,:start_link,[sock,peer,port,id,token,options]},:temporary,:infinity,:worker,:dynamic})
  end 

  def handle_info({:send,{path,payload}}, _from, state) do
    observers = :ets.lookup :resource_observe,path
    Enum.map( observers, fn({endpoint,pid}) -> send pid,payload end)
    {:noreply, state}
  end 
end
