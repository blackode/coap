defmodule Aadya.Message do 

  @version 1
  @max_message_id 65535
  @max_block 1024
  require Record

  use Aadya.Header
  use GenServer
  import Logger

  Record.defrecord :state, [socket: nil, peer: nil, port: nil]
  
  def start_link(module) do
    Application.put_env(:server,:application,module)
    GenServer.start_link(__MODULE__, module, [])
  end

  def init(_) do
    {:ok, nil}
  end

  def handle_info(state) do
    :poolboy.checkin(:message,self())
    {:noreply, state}
  end

  @doc """
  Coap Packet binMessage
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |Ver| T |  TKL  |      Code     |          Message ID           |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |   Token (if any, TKL bytes) ...
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |   Options (if any) ...
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |1 1 1 1 1 1 1 1|    Payload (if any) ...
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  """
  # % incoming CON(0) or NON(1) request
  def handle_cast({:in, {socket,peer,port, binMessage } }, state) do
    parsing binMessage,peer,port,socket
    :poolboy.checkin(:message,self())
    {:noreply, state}
  end

  # incoming RST with empth 
  # Potential case of unobserve

  def handle_cast({:in, {socket,peer,port,<<1::2, 1::1, 1::1, _::bytes>> = binMessage } }, state) do
    observer_stop(binMessage,peer, port,socket)
    :poolddboy.checkin(:message,self())
    {:noreply, state}
  end

  @doc """
  
    Message Parsing 
  """
  def parsing( binMessage, peer, port ,socket) do
    message = Aadya.MessageParser.decode(binMessage)
    coap_message(type: type,method: method, id: id,token: token, options: options) = message
    client = state( socket: socket, port: port, peer: peer)
    # IO.inspect message 
    Logger.info "#{inspect peer} #{port} #{method} #{type} #{id}" 
    case method do
      :nil ->
        nil_methods(message, client)
      :get -> 
      case options[:observe] do
        0 ->
          observer_start(message, client)
        _ ->
          response_get(message, client)
      end
      :put ->
        response_put(message,client)
      :post ->
        response_post(message, client)
      :delete ->
        response_delete(message, client)
    end
  end

  @doc"""
  Response to ACK to PING / Null request

  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
  |Ver| T |  TKL  |      Code     |          Message ID           |
  +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

  """

  def nil_methods( message, client ) do
    coap_message(type: type, id: id,token: token, options: options) = message
    case type do
      :con ->
        _msg_send( client, coap_message(message, type: :reset, token: token))
      :non ->
        coap_message(message, type: :reset, id: id,token: token, options: options) 
      :ack ->
        check_for_observer_counter(id,message)
      :reset ->
        coap_message(message, type: :reset, id: id,token: token, options: options) 
     end
  end

  @doc"""
  Response to Clients using UDP server
  """
  def udp_send(state( socket: socket, peer: peer, port: port ) = client,data) do
    :ok = :gen_udp.send(socket, peer, port,data)
  end 

  #################################################################

  @doc"""
  
  Response to GET Request

  """
  def response_get(coap_message(id: id,token: token, options: options)=message, client) do
    case (options[:block2]) do
      {seq,false,size} ->
        case Aadya.Buffer.get( client, seq, size) do
          [] ->
            _response_get( client, message, options)
          [{pay, bool}] ->
            resp =  bool && :continue || :content
            _msg_send(client, coap_message( message,type: :ack, method: {:ok, resp}, payload: pay, options: [{:block2, {seq, bool,size} }] ))
        end
      :nil->
        _response_get( client, message, options)
      end
  end

  def _response_get( client,message, options) do
    case apply(Application.get_env(:server,:application),:resource_get,[uri_path(options)] ) do
      {method, :nil} ->
        _msg_send(client, coap_message( message, type: :ack, method: method))
      {method,payload } ->
         case options[:block2] do
          {seq, bool, size}->
            if( byte_size(payload) > size ) do
              Aadya.Buffer.start( client, payload)
              [{pay,bool}] =Aadya.Buffer.get( client, seq, size)
              _msg_send(client, coap_message( message, type: :ack, method: {:ok, :content}, payload: pay, options: [{:block2, {seq, bool,size} }]))
            else
              _msg_send(client, coap_message( message, type: :ack, method: {:ok, :content}, payload: payload))
              end
          :nil->
           _msg_send(client, coap_message( message, type: :ack, method: {:ok, :content}, payload: payload))
         end
    end
  end 

  @doc"""

  Response to POST Request

  """

  def response_post(coap_message(id: id,options: options, payload: payload)=message, client) do
     case (options[:block1]) do
      {seq,bool,size} ->
       case :ets.lookup :buffer,client  do
        [] ->
          Aadya.Buffer.start( client, payload)
          _msg_send( client, coap_message(id: id, type: :ack, method: {:ok, :continue }, options: options))
        [{_,pay}] ->
          if bool do
            Aadya.Buffer.append( client,payload)
            _msg_send( client, coap_message(id: id, type: :ack, method: {:ok, :continue }, options: options))
          else
            _response_post(client, id, options, Aadya.Buffer.get_all(client) <> payload)
          end
       end
      nil->
      _response_post( client, id, options ,payload)
    end
  end
def _response_post( client, id, options, payload) do
  case apply(Application.get_env(:server,:application),:resource_post,[uri_path(options),payload] ) do
    {method,_ } ->
      IO.inspect method
      _msg_send( client, coap_message( id: id, type: :ack, method: method ))
    _ ->
      Logger.error "resource put error"
  end
end
  @doc"""

  Response to PUT Request

  """

  def response_put(coap_message(id: id,token: token, options: options, payload: payload)=message,client) do
    case (options[:block1]) do
      {seq,bool,size} ->
       case :ets.lookup :buffer,client  do
        [] ->
          Aadya.Buffer.start( client, payload)
          _msg_send( client, coap_message(id: id, type: :ack, method: {:ok, :continue }, options: options))
        [{_,pay}] ->
          if bool do
            Aadya.Buffer.append( client,payload)
            _msg_send( client, coap_message(id: id, type: :ack, method: {:ok, :continue }, options: options))
          else
            _response_put(client, id, options, Aadya.Buffer.get_all(client) <> payload)
          end
       end
      nil->
      _response_put( client, id, options ,payload)
  end
end

def _response_put( client, id, options, payload) do
  case apply(Application.get_env(:server,:application),:resource_put,[uri_path(options),payload] ) do
    {method,_ } ->
      IO.inspect method
      _msg_send( client, coap_message( id: id, type: :ack, method: method ))
    _ ->
      Logger.error "resource put error"
  end
end

  @doc"""

  Response to DELETE Request

  """

  def response_delete(coap_message(id: id,token: token, options: options)=message, client) do
    case apply(Application.get_env(:server,:application),:resource_delete,[uri_path(options)]) do
      {method,:nil} ->  
        msg_send( client, coap_message( message, type: :ack, method: method))
    end
  end

  ##############
  ## Observer ##
  ##############


  def observer_start(coap_message(options: options, id: id, token: token )=message,  client) do
    uri_resource = uri_path options
    unobservers = Application.get_env(:server,:application).unobserve_resources
    # case unobservers do
    #   []->
    #       _observer_start(socket,peer,port,id,token,options,uri_resource) 
    #   _->
        # case Enum.find(unobservers, fn(_unob) ->  _unob == uri_resource end )do
          # nil ->
            # _observer_start(socket,peer,port,id,token,options,uri_resource)
          # item ->
              # udp_send( clien,message_decode(coap_message(type: :ack, method: {:error, :forbidden},id: id,token: token ,options: options)))
        # end
    # end
  end

  defp _observer_start(socket,peer,port,id,token,options,uri_resource) do
    case Aadya.ObserverSup.observer_start(socket,peer,port,id,token,options) do
      {:ok, pid}->
        case :ets.lookup(:resource_observer,uri_resource) do
          [] ->
            :ets.insert(:resource_observer_pid, {pid, {uri_resource,peer,port,id,token,options}} )
            :ets.insert(:resource_observer, {uri_resource, [pid]})
          [{_path,observers_list}] ->
            :ets.insert(:resource_observer_pid, {pid, {uri_resource,peer,port,id,token,options}} )
            :ets.insert(:resource_observer, {uri_resource, observers_list ++ [pid] } )
          end
          msg_send( client,coap_message(type: :ack, method: {:ok, :content},id: id,token: token ,options: options))
            Logger.info "Observer #{inspect peer} #{inspect port}" 
      {:error,_error}->
        Logger.warn "Observer error #{inspect peer} #{inspect port}" 
      end
  end

  def check_for_observer_counter( msg_id, message) do
    case :ets.lookup( :resource_observer_msg_id, msg_id) do
      [ {id, [ pid, ref ] } ] ->
        :ets.delete( :resource_observer_msg_id, msg_id)
         send pid,{:counter_clear,ref} 
    end
  end

  def observer_stop(coap_message(id: msgid)=message, peer, port, socket) do
    pid = Aadya.StorageAdapter.observer_find(msgid)
    Supervisor.delete_child Aadya.ObserverSup,pid
    Logger.info "Observer stop #{inspect message}"
    observer_delete(pid)
  end

  def observer_delete(observer_pid) do
    [{pid,{point,_,_,_,_,_}}] = :ets.lookup(:resource_observer_pid,observer_pid)
    [{_,pids_list}] = :ets.lookup(:resource_observer,point)
    :ets.update_element(:resource_observer,point, List.delete(pids_list,pid))  
    :ets.delete(:resource_observer_pid,pid)  
  end


  #########
  # Utils #
  #########
  
  @doc """
  URI path to REST style format

  [uri_path: ["asdf", "23"]] -- > /asdf/23

  """
  def uri_path(options) do
    case options[:uri_path] do
      :nil->
        "/"
      path->
        "/"<>Enum.join path,"/"
    end
  end

  @doc """
  Request Query

  coap://10.0.0.5:5683/kill/df?df&as

  [uri_query: ["df", "as"], uri_path: ["kill", "df"]], ""}

  """
  def uri_query(options) do
    options[:uri_query]
  end


  @doc """
  Decoding Binary to CoapMessage Record

  CoapMessageParser.decode <<64, 0, 14, 246>>
  {:coap_message, :con, nil, 3830, "", [], ""}    
  """
  def message_decode(message) do
    Aadya.MessageParser.encode message
  end

  defp _msg_send( client, message) do
    udp_send( client,
             Aadya.MessageParser.encode(message)
           )
  end

  defp msg_send( client, message) do
    udp_send(client,
             Aadya.MessageParser.encode(message)
           )
  end

  defp msg_send(client, message, options) do
    udp_send(client,
             Aadya.MessageParser.encode(
                                         message,
                                         type: :ack,
                                         method: {:ok, :continue},
                                         options: options 
                       )
                     )
  end
end
