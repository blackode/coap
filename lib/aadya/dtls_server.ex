defmodule Aadya.DtlsServer do

	require Record
	Record.defrecord :state,[sock: :nil,supid: :nil,channel: :nil]

	def connect(host,port) do
		{:ok, socket}  = GenServer.start_link __MODULE__, [:connect, host, port],[]
		{:ok, channel} = get_channel(socket,{host,port})
		{:ok, socket, channel}
	end

	def close(pid) do
		GenServer.cast(pid,:shutdown)
	end

	def start_link(listen_socket) do
		GenServer.start_link(__MODULE__, [:accept, listen_socket], [])
	end

	def get_channel(pid, chid) do
		GenServer.call(pid, {:get_channel, chid})
	end

	def init([:connect, host, port]) do
		{:ok, sock} = :ssl.connect host,port,[:binary,{:protocol,:dtls}]
		{:ok, state(sock: sock)}
	end

	def init([:accpet, listensocket])do
		GenServer.cast self(),:accpet
		{:ok, state(sock: listensocket)}
	end

	def handle_call({:get_channel,chid}, _from, state(channel: nil)=state ) do
		{:ok, suppid, pid} = CoapChannelSup.start_link(self(), chid)
    	{:reply, {:ok, pid}, state(state,[supid: suppid, channel: pid])}
	end

	def handle_cast(:accept, state(sock: listensocket)=state) do
    	{:ok, socket} = :ssl.transport_accept(listensocket)
    	# % create a new acceptor to maintain a set of waiting acceptors
	    CoapDtlsListen.start_socket()
	    # % establish the connection
	    :ok = :ssl.ssl_accept(socket)
	    # % FIXME: where do we get the chanel id?
	    {:ok, supPid, pid} = CoapChannelSup.start_link(self(), {{0,0,0,0}, 0})
	    {:noreply, state(state,[sock: socket, supid: supPid, channel: pid])}
	end

	def handle_cast(:shutdown, state) do
    	{:stop, :normal, state}
    end

	def handle_info({:ssl, _socket, data}, state(channel: chan) = state) do
    	send chan,{:datagram, data}
    	{:noreply, state}
    end

	def handle_info({:ssl_closed, _socket}, state) do
    	{:stop, :normal, state}
    end

	def handle_info({:datagram, _chid, data}, state(sock: socket)) do
    	:ok = :ssl.send( socket, data)
    	{:noreply, state}
	end

	def handle_info({:terminated, suppid, _chid}, state(sock: socket, supid: suppid)=state) do
    # % the channel has terminated
    	:ssl.close(socket)
    	{:stop, :normal, state}
	end
	
	def handle_info(info, state) do
    	Io.fwrite("coap_dtls_socket unexpected ~p~n", [info])
    	{:noreply, state}
	end

	def code_change(_oldVsn, state, _extra) do
    	{:ok, state}
    end

	def terminate(_reason, _state) do
	    # Io.fwrite("coap_dtls_socket terminated ~w~n", [reason])
    	:ok
    end
end
