defmodule Coap.Mixfile do
  use Mix.Project

  def project do
    [app: :aadya,
     version: "0.1.0",
     elixir: "~> 1.3",
     compilers: [:phoenix] ++ Mix.compilers,
     elixirc_paths: elixirc_paths(Mix.env),
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     aliases: aliases(),
     deps: deps()]
  end

  def application do
    [
      applications: [:logger,:poolboy, :phoenix, :phoenix_html, :cowboy, :logger_papertrail_backend],
      extra_applications: [:logger]
    ]
  end

  defp elixirc_paths(_),     do: ["lib", "web"]

  defp deps do
    [
     {:poolboy, "~> 1.5.1"},
     {:logger_papertrail_backend, "~> 0.2.1"},
     {:credo, "~> 0.6.1", only: [:dev, :test]},
     {:phoenix, "~> 1.2.1"},
     {:phoenix_html, "~> 2.6"},
     {:ex_doc, "~> 0.14", only: :dev, runtime: false},
     {:poison, "~> 2.2.0"},
     {:cowboy, "~> 1.0"}
   ]
  end

  defp aliases do
    ["c": ["compile","coap.on"]]
  end
end
