defmodule Aadya.PageController do
  use Aadya.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
